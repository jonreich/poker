defmodule Poker.Room do
  defstruct voters: nil

  def new() do
    %Poker.Room{
      voters: Map.new()
    }
  end

  def send_update(room) do
    broadcast(room, :update)
  end

  def event(:join, room_id, socket_id) do
    {:ok, updated_room} = Poker.RoomManager.join_room(room_id, socket_id)
    broadcast(%{room_id: room_id, room: updated_room}, :update)
  end

  def event(:leave, room_id, socket_id) do
    {:ok, updated_room} = Poker.RoomManager.leave_room(room_id, socket_id)
    broadcast(%{room_id: room_id, room: updated_room}, :update)
  end

  def event(:vote, room_id, socket_id, vote) do
    {:ok, updated_room} = Poker.RoomManager.vote(room_id, socket_id, vote)
    broadcast(%{room_id: room_id, room: updated_room}, :update)
  end

  def event(:reset, room_id) do
    {:ok, room} = Poker.RoomManager.find_room(room_id)

    Enum.each(Map.keys(room.voters), fn key ->
      Poker.RoomManager.vote(room_id, key, nil)
    end)

    {:ok, updated_room} = Poker.RoomManager.find_room(room_id)

    broadcast(%{room_id: room_id, room: updated_room}, :update)
  end

  defp broadcast({:error, _reason} = error, _event), do: error

  defp broadcast(message, event) do
    Phoenix.PubSub.broadcast(Poker.PubSub, "#{message.room_id}", {event, message})
    {:ok, message}
  end

  def subscribe(id) do
    Phoenix.PubSub.subscribe(Poker.PubSub, id)
  end
end
