defmodule Poker.RoomManager do
  use Agent

  def start_link(_) do
    Agent.start_link(fn -> Map.new() end, name: __MODULE__)
  end

  def find_room(id) do
    case Agent.get(__MODULE__, &Map.fetch(&1, id)) do
      :error ->
        :error

      {:ok, room} ->
        {:ok, room}
    end
  end

  def create_room() do
    new_id = generate_id()
    Agent.update(__MODULE__, &Map.put(&1, new_id, Poker.Room.new()))
    {:ok, new_id}
  end

  def list_rooms() do
    Agent.get(__MODULE__, & &1)
  end

  def join_room(id, voter_id) do
    Agent.get_and_update(
      __MODULE__,
      &Map.get_and_update(&1, id, fn current ->
        {:ok,
         %{current | voters: Map.put(current.voters, voter_id, %Poker.Voter{current_vote: nil})}}
      end)
    )

    Agent.get(__MODULE__, &Map.fetch(&1, id))
  end

  def leave_room(id, voter_id) do
    Agent.get_and_update(
      __MODULE__,
      &Map.get_and_update(&1, id, fn current ->
        {:ok, %{current | voters: Map.delete(current.voters, voter_id)}}
      end)
    )

    {:ok, room} = Agent.get(__MODULE__, &Map.fetch(&1, id))

    if Kernel.map_size(room.voters) === 0 do
      Agent.get_and_update(__MODULE__, &Map.delete(&1, id))
    else
      {:ok, room}
    end
  end

  def vote(id, voter_id, vote) do
    Agent.get_and_update(
      __MODULE__,
      &Map.get_and_update(&1, id, fn current ->
        {:ok,
         %{
           current
           | voters:
               Map.update(current.voters, voter_id, nil, fn current_vote ->
                 %{current_vote | current_vote: vote}
               end)
         }}
      end)
    )

    Agent.get(__MODULE__, &Map.fetch(&1, id))
  end

  defp generate_id do
    :crypto.strong_rand_bytes(8) |> Base.url_encode64() |> binary_part(0, 8)
  end
end
