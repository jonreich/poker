defmodule PokerWeb.RoomLive do
  use PokerWeb, :live_view
  import Poker.RoomManager

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    case connected?(socket) do
      true ->
        case find_room(id) do
          :error ->
            {:ok, redirect(socket, to: "/")}

          {:ok, room} ->
            Poker.Room.subscribe(id)
            Poker.Room.event(:join, id, socket.id)

            {:ok,
             assign(
               socket,
               room_id: id,
               room: room,
               loading: false,
               allvoted: false,
               sequence: [1, 2, 3, 5, 8, 13]
             )}
        end

      false ->
        {:ok, assign(socket, loading: true)}
    end
  end

  @impl true
  def handle_event("vote", %{"value" => value}, socket) do
    if value != "" do
      case Poker.Room.event(:vote, socket.assigns.room_id, socket.id, value) do
        {:ok, _} ->
          {:noreply, socket}
      end
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_event("reset", _, socket) do
    case Poker.Room.event(:reset, socket.assigns.room_id) do
      {:ok, _} ->
        {:noreply, socket}
    end
  end

  @impl true
  def terminate(_reason, socket) do
    if socket.assigns.room_id, do: Poker.Room.event(:leave, socket.assigns.room_id, socket.id)
  end

  @impl true
  def handle_info({:update, %{room: room}}, socket) do
    {:noreply,
     assign(
       socket,
       room: room,
       allvoted: length(Enum.filter(Map.values(room.voters), &is_nil(&1.current_vote))) === 0
     )}
  end
end
