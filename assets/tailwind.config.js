module.exports = {
  purge: [
    '../lib/**/*.ex',
    '../lib/**/*.leex',
    '../lib/**/*.eex',
    './js/**/*.js'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        teal: {
          300: '#4db6ac',
          400: '#26a69a',
          500: '#009688',
          600: '#00897b'
        },
        bluegray: {
          300: '#3f4a5c',
          400: '#2B3648',
          700: '#212936'
        }
      },
      borderWidth: {
        '16': '16px'
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
